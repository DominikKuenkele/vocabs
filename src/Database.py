import csv
import sqlite3

from Course import Course
from Word import Word


class Database:
    connection = []
    cursor = []

    def __init__(self):
        self.connection = sqlite3.connect('../test.db')
        self.cursor = self.connection.cursor()

    def init_database(self):
        self.cursor.execute('CREATE TABLE IF NOT EXISTS vocabulary (name varchar(50), definition varchar(50))')

    def add_vocabs_from_csv(self, csvfile):
        csv_reader = csv.reader(csvfile, delimiter=';')
        for row in csv_reader:
            start_box = self.get_course(row[3]).get_start_box()

            word = Word(self.connection)
            word.definition = row[0]
            word.description = row[1]
            word.additional_info = row[2]
            word.box_id = start_box.id
            word.persist()

    def print_database(self):
        self.cursor.execute('select * from courses')
        print(self.cursor.fetchall())
        self.cursor.execute('select * from boxes')
        print(self.cursor.fetchall())
        self.cursor.execute('select * from vocabulary')
        print(self.cursor.fetchall())

    def get_course(self, course_name):
        self.cursor.execute('SELECT id, name FROM courses WHERE name = ?', [course_name])
        result = self.cursor.fetchone()

        course = Course(self.connection)
        course.id = result[0]
        course.name = result[1]

        return course
