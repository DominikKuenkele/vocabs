import abc


class Persistable(abc.ABC):
    @abc.abstractmethod
    def persist(self):
        pass
