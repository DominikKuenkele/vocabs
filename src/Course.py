from Box import Box
from Persistable import Persistable


class Course(Persistable):
    id = -1
    name = ''

    def __init__(self, connection):
        self.db_connection = connection
        self.db_cursor = self.db_connection.cursor()

    def persist(self):
        if id == -1:
            self.db_cursor.execute("INSERT INTO courses(name) VALUES (?)",
                                   [self.name])
        else:
            self.db_cursor.execute("UPDATE courses SET name=? WHERE id = ?",
                                   [self.name, self.id])
        self.db_connection.commit()

    def get_boxes(self):
        self.db_cursor.execute('SELECT * FROM boxes WHERE course_id=?', [self.id])

        result = self.db_cursor.fetchall()
        boxes = []
        for row in result:
            box = Box(self.db_connection)
            box.id = row[0]
            box.name = row[1]
            box.next_box = row[2]
            box.course_id = row[3]
            box.box_type = row[4]
            boxes.append(box)

        return boxes

    def get_start_box(self):
        self.db_cursor.execute('SELECT * FROM boxes WHERE course_id=? AND box_type=?', [self.id, 'start'])

        result = self.db_cursor.fetchone()
        box = Box(self.db_connection)
        box.id = result[0]
        box.name = result[1]
        box.next_box = result[2]
        box.course_id = result[3]
        box.box_type = result[4]

        return box
