from Persistable import Persistable
from Word import Word


class Box(Persistable):
    id = -1
    name = ''
    next_box = 0
    course_id = 0
    box_type = ''

    def __init__(self, connection):
        self.db_connection = connection
        self.db_cursor = self.db_connection.cursor()

    def __repr__(self):
        return str(self.id) + ' ' + self.name + ' ' + str(self.next_box) + ' ' + str(
            self.course_id) + ' ' + self.box_type

    def persist(self):
        if id == -1:
            self.db_cursor.execute("INSERT INTO boxes(name, next_box, course_id, box_type) VALUES (?, ?, ?, ?)",
                                   [self.name, self.next_box, self.course_id, self.box_type])
        else:
            self.db_cursor.execute("UPDATE boxes SET name=?, next_box=?, course_id=?, box_type=? WHERE id = ?",
                                   [self.name, self.next_box, self.course_id, self.box_type, self.id])
        self.db_connection.commit()

    def get_vocabulary(self):
        self.db_cursor.execute('SELECT * FROM vocabulary WHERE box_id=?', [self.id])

        result = self.db_cursor.fetchall()
        vocabulary = []
        for row in result:
            word = Word(self.db_connection)
            word.id = row[0]
            word.definition = row[1]
            word.description = row[2]
            word.box_id = row[3]
            word.additional_info = row[4]
            vocabulary.append(word)

        return vocabulary
