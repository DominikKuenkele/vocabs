from Persistable import Persistable


class Word(Persistable):
    id = -1
    definition = ''
    description = ''
    additional_info = ''
    box_id = 0

    def __init__(self, connection):
        self.db_connection = connection
        self.db_cursor = self.db_connection.cursor()

    def persist(self):
        if self.id == -1:
            self.db_cursor.execute(
                "INSERT INTO vocabulary(definition, description, box_id, addtional_info) VALUES (?, ?, ?, ?)",
                [self.definition, self.description, self.box_id, self.additional_info])
        else:
            self.db_cursor.execute(
                "UPDATE vocabulary SET definition=?, description=?, box_id=?, addtional_info=? WHERE id = ?",
                [self.definition, self.description, self.box_id, self.additional_info, self.id])
        self.db_connection.commit()

    def move_to(self, box_id):
        self.box_id = box_id
        self.persist()
