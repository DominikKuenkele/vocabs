import random

from Database import Database
from termcolor import colored


class LearningSession:
    start_box_queue = []

    def __init__(self):
        self._db = Database()

    def start_all(self, course_name):
        course = self._db.get_course(course_name)
        boxes = course.get_boxes()
        sorted_boxes = self.__sort_boxes__(boxes)

        for box in sorted_boxes:
            vocabs = box.get_vocabulary()
            random.shuffle(vocabs)
            if len(vocabs) != 0:
                print(colored('Box:', 'cyan', attrs=['underline', 'bold']),  box.name)
                for vocab in vocabs:
                    self.__evaluate_vocab__(vocab, box)

        for vocab in self.start_box_queue:
            vocab.move_to(self.start_box.id)

    def __sort_boxes__(self, boxes):
        sorted_boxes = []
        for box in boxes:
            if box.box_type == 'start':
                sorted_boxes.append(box)
                self.start_box = box
                break
        while len(sorted_boxes) < len(boxes):
            for box in boxes:
                if sorted_boxes[-1].next_box == box.id:
                    sorted_boxes.append(box)
        sorted_boxes.reverse()
        return sorted_boxes

    def __evaluate_vocab__(self, vocab, box):
        print(colored(vocab.definition, 'magenta'), end='')
        input('')
        print(' - ' + vocab.description)
        if vocab.additional_info != '':
            print(' - ' + vocab.additional_info)
        answer = input('Enter y, when correct')
        if answer == 'y':
            if box.box_type != 'end':
                vocab.move_to(box.next_box)
        else:
            self.start_box_queue.append(vocab)
